#ifndef HR_SR04_H
#define HR_SR04_H

namespace Samonzeweb {
  class HR_SR04 {
    public:
      HR_SR04(int initPinTrigger, int initPinEcho);
      float DistanceCm();

      // Two measures does not be within less than 50ms
      // As it is the minimum time and a measure can force
      // wait time, it could be an useful (public) information.
      static const unsigned long DELAY_BETWEEN_MEASURES_MS = 50;

    private:
      // Used pin for sensor
      int triggerPin;
      int echoPin;

      // Last measure time
      unsigned long lastMeasureTime;

      // The sound speed is approximatively 29.4 µs/cm (340 m/s)
      // We need to double for the full round trip
      static const float MICROSEC_PER_CM = 58.8;
  };
}

#endif
