#include <Arduino.h>
#include "HC_SR04.h"

namespace Samonzeweb {
  HR_SR04::HR_SR04(int initTriggerPin, int initEchoPin) {
    triggerPin = initTriggerPin;
    echoPin = initEchoPin;
    // Hardware initialization
    pinMode(triggerPin, OUTPUT);
    pinMode(echoPin, INPUT);
    digitalWrite(triggerPin, LOW);
    // No measure
    lastMeasureTime = 0;
  }

  float HR_SR04::DistanceCm() {
    // Need wait time ?
    unsigned long duration = millis() - lastMeasureTime;
    if(duration < DELAY_BETWEEN_MEASURES_MS)
      delay(DELAY_BETWEEN_MEASURES_MS - duration);

    // Measure order
    digitalWrite(triggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(triggerPin, LOW);

    // Wait for echo, value in µs
    unsigned long roundTripTime = pulseIn(echoPin, HIGH);
    lastMeasureTime = millis();
    return roundTripTime / MICROSEC_PER_CM;
  }
}
