# Overview

It's a simple class to manage HC_SR04 ultrasonic sensor.

# Example

```
#include <HC_SR04.h>

const byte trigPin = 13;
const byte echoPin = 12;

using namespace Samonzeweb;
HR_SR04 sonar(trigPin, echoPin);

void setup() {
  Serial.begin(9600);
}

void loop() {
  float distance;

  distance = sonar.DistanceCm();
  Serial.print("Distance (cm) : ");
  Serial.println(distance);
  delay(1000);
}
```
